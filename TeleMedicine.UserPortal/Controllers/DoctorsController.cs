﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Alpha.User.Models;

namespace Alpha.UserPortal.Controllers
{
    public class DoctorsController : Controller
    {

        private MedicContext dc = new MedicContext();
        // GET: Doctors
        public ActionResult Index(int page=0,int size=20)
        {

            var doctors = dc.DoctorProfiles.OrderBy(i => i.Name).Skip(page * size).ToList();
            return View();
        }
    }
}