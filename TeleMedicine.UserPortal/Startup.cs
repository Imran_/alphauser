﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Alpha.UserPortal.Startup))]
namespace Alpha.UserPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
