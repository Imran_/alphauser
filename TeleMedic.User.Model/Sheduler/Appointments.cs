﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alpha.User.Models;

namespace Alpha.User.Model.Sheduler
{
    public class Appointments
    {
        public Guid Id { get; set; }

        public string PaitentId { get; set; }
        public virtual UserTable Paitent { get; set; }

        public string DoctorId { get; set; }
        public virtual UserTable Doctor { get; set; }

        public DateTime On { get; set; }
        public bool Done { get; set; }
        public string DoctorNotes { get; set; }
        public string PaitentNotes { get; set; }
    }
}
