﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alpha.User.Model.Sheduler
{
    public class Slots
    {
        public Guid Id { get; set; }

        public string Paitent { get; set; }
        public string Doctor { get; set; }
        public DateTime On { get; set; }
        public bool Approved { get; set; }
    }
}
