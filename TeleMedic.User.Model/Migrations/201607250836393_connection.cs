namespace Alpha.User.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class connection : DbMigration
    {
        public override void Up()
        {
            
            
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionID = c.String(nullable: false, maxLength: 128),
                        UserAgent = c.String(),
                        Connected = c.Boolean(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ConnectionID)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
           
            
          
         
        
         
          
            
         
            
        }
        
        public override void Down()
        {
           
            DropForeignKey("dbo.Connections", "UserId", "dbo.Users");
           
        }
    }
}
