﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alpha.User.Models;

namespace Alpha.User.Model.Paitent
{
    public class PaitentActivty
    {
        public Guid Id { get; set; }
        public string UserTableId { get; set; }
        public virtual UserTable User { get; set; }
        public DateTime On { get; set; }
        public string Event { get; set; }
    }
}
