﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Alpha.User.Model.Doctor;
using Alpha.User.Model.Paitent;
using Alpha.User.Model.Sheduler;
using System.Data.Entity.ModelConfiguration.Conventions;
using Alpha.User.Model;
using System.Collections.Generic;

namespace Alpha.User.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class UserTable : IdentityUser
    {
        public virtual ICollection<Connection> Connections { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserTable> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class MedicContext : IdentityDbContext<UserTable>
    {
        public MedicContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public DbSet<Notes> Notes { get; set; }
        public DbSet<PaitentProfile> PatientProfiles { get; set; }
        public DbSet<DoctorProfile> DoctorProfiles { get; set; }
        public DbSet<Slots> Slots { get; set; }
        public DbSet<Appointments> Appointments { get; set; }
        public DbSet<PaitentActivty> PatientsActivities { get; set; }
        public DbSet<DoctorActivity> DoctorsActivities { get; set; }
        public DbSet<Connection> Connections { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // This needs to go before the other rules!
          //  modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<UserTable>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
        }
        public static MedicContext Create()
        {
            return new MedicContext();
        }

        
    }
}