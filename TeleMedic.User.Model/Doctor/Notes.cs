﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alpha.User.Model.Doctor
{
    public class Notes
    {
        public Guid Id { get; set; }
        public string Note { get; set; }
        public string PatientUser { get; set; }
        public string DocUser { get; set; }
        public DateTime On { get; set; }

    }
}
